import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"packages": ["os"],
					 "excludes": ["tkinter"]}

bdist_mac_options = {"bundle-iconfile" : ["TemplateIcon.icns"]}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(  name = "RAW",
        version = "0.99.11",
        description = "RAW",
        options = {"build_exe": build_exe_options,
				   "bdist_mac": bdist_mac_options},
        executables = [Executable("RAW.py", base=base)])